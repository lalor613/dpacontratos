<?php include("../administrador/templates/cabecera.php"); ?>

<div class="alert alert-success col-md-12" role="alert">
    <h4 class="alert-heading">Bienvenido:</h4>
    <p class="mb-0"><?php echo $_SESSION['nombre']; ?></p>
    <p class="mb-0"><?php echo $_SESSION['correo']; ?></p>
</div>
<div class="col-md-12">

    <div class="jumbotron">
        <h1 class="display-3">Dependencias</h1>
        <p class="lead">Administrador de contratos para el ciclo escolar ..</p>
        <hr class="my-2">
        <p>...</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Empezar</a>
        </p>
    </div>

</div>

<?php include("../administrador/templates/pie.php"); ?>