<?php if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $correo= filter_var($_POST['correo'], FILTER_SANITIZE_EMAIL);
    $contraseña = $_POST['contraseña'];
     $errores = '';
    if (empty($correo) or empty($contraseña)) {
        $errores .= '<li> Llena todos los campos </li>';
    } else {
        try {
            $conexion = pg_connect("host=localhost port=5432 dbname=pruebaV user=postgres password=admin");
        } catch (PDOException $e) {
            echo "ERROR:" . $e->getMessage();
        }
        $statement = pg_prepare($conexion, "user_exist", "SELECT * FROM usuarios where correo = \$1  AND contraseña = \$2 ");
        $contraseña = hash('sha512', $contraseña);
        $statement = pg_execute($conexion, "user_exist", array($correo,  $contraseña ));
        $resultado = pg_fetch_assoc($statement);
        print_r($resultado);
        if ($resultado !== False ){
            session_start();
            $_SESSION['correo'] = $correo;
            $buser= pg_prepare($conexion, "tipo_user", "SELECT * FROM usuarios WHERE correo = \$1 ");
            $buser = pg_execute($conexion, "tipo_user", array($correo));
            $result2 = pg_num_rows($buser);
            if($result2 == 1){
                while($row = pg_fetch_assoc($buser)){
                    $_SESSION['nombre']=$row['nombre'];
                    $_SESSION['tipo_id']=$row['tipo_id'];
                    $_SESSION['depe_user']=$row['depe_user'] ;
                }
            }

            if ($_SESSION['tipo_id']==1){
                header("Location: ../administrador/inicio.php");
            }elseif($_SESSION['tipo_id']==2){
                header("Location: ../inicio.php ");
            }else{
                header("Location: ../administrador/index.php");
            }
        }else{
            $errores.='<li> Datos incorrectos </li>';
        }
    }
} 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INICIO DE SESION</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <div class="row">

            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        INICIAR SESION
                    </div>
                    <div class="card-body">

                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" name="login">
                            <div class="form-group">
                                <label>Correo</label>
                                <input type="email" name="correo" class="form-control" placeholder="@unach.mx">
                            </div>
                            <div class="form-group">
                                <label>contraseña</label>
                                <input type="password" name="contraseña" class="form-control" placeholder="contraseña">
                            </div>

                            <button type="submit" class="btn btn-primary" onclick="login.submit()">Ingresar</button>
                            <?php if (!empty($errores)) : ?>
                                <p class="form-text text-muted">
                                    <?php echo $errores; ?>
                                </p>
                            <?php endif; ?>

                        </form>
                    </div>

                    <p class="form-text text-muted align-self-center">
                        ¿Aún no tienes cuenta?
                    </p>
                    <small class="text-muted align-self-center">Comunicate a con tu administrador de la DGDSE</small>
                    <br>
                </div>
            </div>
        </div>
    </div>


</body>

</html>