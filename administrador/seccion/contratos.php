<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';

$cicloesc = pg_query($conn, "SELECT * FROM ciclos_escolares");
$asignadoseva = pg_query($conn, "SELECT * FROM view_asignados_eva");
$asignadostitu = pg_query($conn, "SELECT * FROM view_asignados_titu");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $fkeva = (isset($_POST['fkeva'])) ? $_POST['fkeva'] : "";
    $fkeva_depe = (isset($_POST['fkeva_depe'])) ? $_POST['fkeva_depe']: "";
    $fkeva_ciclo = (isset($_POST['fkeva_ciclo'])) ? $_POST['fkeva_ciclo']:"" ;

    $fktitu = (isset($_POST['fktitu'])) ? $_POST['fktitu'] : "";
    $fktitu_depe = (isset($_POST['fktitu_depe'])) ? $_POST['fktitu_depe']:"";
    $fktitu_ciclo = (isset($_POST['fktitu_ciclo'])) ? $_POST['fktitu_ciclo']:"";

    $id_tipo = (isset($_POST['id_titu'])) ? $_POST['id_titu']:"";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    switch ($accion) {
        case 'Agregar':
            
            break;
    }
}




?>

<div class="col-md-5">
    <div class="card">
        <div class="card-header">
            Creacion contratos
        </div>

        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="card-body">
                <div class="form-row">
                    <div class="col form-group">
                        <label for="fkciclo_depe">Ciclo escolar</label>
                        <select class="form-control custom-select" name="fkciclo_depe" id="fkciclo_depe">
                            <option selected value="0">--Elige ciclo escolar--</option>
                            <?php
                            while ($fila = pg_fetch_array($cicloesc)) {
                                echo "<option value =" . $fila['id_ciclo'] . ">" . $fila['periodo_des'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col form-group" id="fkclave_depe">
                        <!-- Aqui se genera el select para las dependencias -->
                    </div>


                </div>

                <div class="form-group">
                    <label for="fkeva">Docente por contratar</label>
                    <select class="form-control custom-select" name="fkeva" id="fkeva">
                        <option selected>--Elige un docente--</option>
                        <?php
                        while ($fila = pg_fetch_array($asignadoseva)) {
                            echo "<option value=" . $fila['plaza_eva_asig'] . ">" . $fila['nombre_eva'] . "</option>";
                        }
                        ?>
                    </select>
                </div>


                <div class="form-group">
                    <label for=""></label>
                    <select class="custom-select" name="" id="">
                        <option selected>--Elige un titular--</option>
                        <?php
                        while ($fila = pg_fetch_array($asignadostitu)) {
                            echo "<option value=" . $fila['plaza_titu_asig'] . ">" . $fila['nombre_titu'] . "</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="submit" name="accion" value="Agregar" class="btn btn-success">Agregar</button>
                </div>

            </div>
        </form>

    </div>
</div>
<div class="col-md-7">

</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#fkciclo_depe').change(function() {
            recargarlista();
        });
    })
</script>
<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/contratossel.php",
            data: "ciclo=" + $('#fkciclo_depe').val(),
            success: function(r) {
                $('#fkclave_depe').html(r);
            }
        })
    }
</script>

<?php include('../templates/pie.php'); ?>