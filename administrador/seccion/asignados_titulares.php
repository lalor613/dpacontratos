<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $plaza_titu_asig = (isset($_POST['plaza_titu_asig'])) ? $_POST['plaza_titu_asig'] : "";
    $depe_titu_asig = (isset($_POST['depe_titu_asig'])) ? $_POST['depe_titu_asig'] : "";
    $ciclo_titu_asig = (isset($_POST['ciclo_titu_asig'])) ? $_POST['ciclo_titu_asig'] : "";
    $id_mate_titu = (isset($_POST['id_mate_titu'])) ? $_POST['id_mate_titu'] : "";

    $accion =  (isset($_POST['accion'])) ? $_POST['accion'] : "";

    switch ($accion) {

        case 'Agregar':
            $insertar = pg_prepare($conn, "insert_asigtitu", "INSERT INTO asignados_titu VALUES ( \$1, \$2, \$3, \$4) ");

            if ($plaza_titu_asig and $id_mate_titu) {
                $verasig = pg_prepare($conn, "exist_asigmate", "SELECT *  FROM  asignados_titu WHERE plaza_titu_asig = \$1  AND id_mate_titu = \$2 ");
                $verasig = pg_execute($conn, "exist_asigmate", array($plaza_titu_asig, $id_mate_titu));
                $resultado = pg_fetch_assoc($verasig);
                if ($resultado == False) {
                    $insertar = pg_execute($conn, "insert_asigtitu", array($plaza_titu_asig, $depe_titu_asig,  $ciclo_titu_asig,  $id_mate_titu));
                    $errores .= "<li> Se Agregó la materia al docente </li>";
                } else {
                    $errores .= "<li> La materia seleccionada ya fue añadida al docente </li>";
                }
            }
            break;

        case 'Borrar':
            $borrar = pg_query($conn, "DELETE FROM asignados_titu WHERE (plaza_titu_asig = '$plaza_titu_asig' AND depe_titu_asig = '$depe_titu_asig' AND ciclo_titu_asig = '$ciclo_titu_asig' AND id_mate_titu='$id_mate_titu')");
            break;
    }
}
$captardepetitu = pg_query($conn, "SELECT * FROM view_titulares_dependencias");
$existtitu = pg_num_rows($captardepetitu);
$dependencia = pg_query($conn, "SELECT * FROM dependencias");
$ciclos = pg_query($conn, "SELECT * FROM ciclos_escolares");
$materias = pg_query($conn, "SELECT * FROM materias");
$mostrarasig = pg_query($conn, "SELECT * FROM view_asignados_titu");
?>

<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>

<div class="col-md-6">
    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

        <div class="card">
            <div class="card-header">
                Elige Docente Titular
            </div>

            <div class="card-body">
                <div class="form-row">

                    <div class="col">
                        <label for="ciclo_titu_asig">Ciclo escolar</label>
                        <select class="form-control" name="ciclo_titu_asig" id="ciclo_titu_asig">
                            <option value=0>--Elige el ciclo--</option>
                            <?php
                            while ($fila = pg_fetch_assoc($ciclos)) {
                                echo "<option value=" . $fila['id_ciclo'] . ">" . $fila['periodo_des'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col form-group" id="depe_titu_asig"> </div>


                </div>

                <div class="form-group">
                    <label for="plaza_titu_asig">Nombre</label>
                    <select class="form-control" name="plaza_titu_asig" id="plaza_titu_asig">
                        <option selected>--Eliga a titular---</option>
                        <?php
                        while ($fila = pg_fetch_assoc($captardepetitu)) {
                            echo "<option value=" . $fila['plaza_titu'] . ">" . $fila['nombre_titu'] . "</option>";
                        }
                        ?>
                    </select>
                </div>

            </div>

            <div class="card-header">
                Elige materia
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="id_mate_titu">Materia(s)</label>
                    <select name="id_mate_titu" multiple class="form-control">
                        <option selected>-Selecciona una materia-</option>
                        <?php
                        while ($fila = pg_fetch_array($materias)) {
                            echo "<option value=" . $fila['id_mate'] . ">" . $fila['nom_materia'] . "\t|\t" . $fila['semestre'] . "\t|\t" . $fila['grupo'] . "\t|\t" . $fila['hsm'] . "</option>";
                        } ?>
                    </select>
                </div>

                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="submit" name="accion" value="Agregar" class="btn btn-success">Agregar</button>
                </div>
            </div>

        </div>

    </form>
</div>

<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            Materias cargadas
            <small class="text-muted">cargado a: (-fulanito-)</small>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-inverse table-responsive">
                <thead class="thead-inverse text-center">
                    <tr>
                        <th>Docente</th>
                        <th>Asignatura</th>
                        <th>Semestre</th>
                        <th>Grupo</th>
                        <th>Carga horaria</th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <tbody class="text-nowrap">
                    <?php
                    while ($fila = pg_fetch_assoc($mostrarasig)) {
                    ?>
                        <tr>
                            <td>
                                <?php echo $fila['nombre_titu']; ?><br>
                                <small class="text-muted"><?php echo $fila['plaza_titu']; ?></small>
                            </td>
                            <td>
                                <small class="text-muted"> <?php echo $fila['plan_est']; ?></small><br>
                                <?php echo 'ID: ' . $fila['id_mate'] . ' > ' . $fila['nom_materia']; ?>

                            </td>
                            <td><?php echo $fila['semestre']; ?></td>
                            <td><?php echo $fila['grupo']; ?></td>
                            <td><?php echo $fila['hsm']; ?></td>
                            <td>
                                <div class="form-group">
                                    <form method="POST">
                                        <input type="hidden" name="depe_titu_asig" value="<?php echo $fila['depe_titu_asig']; ?>">
                                        <input type="hidden" name="ciclo_titu_asig" value="<?php echo $fila['ciclo_titu_asig']; ?>">
                                        <input type="hidden" name="id_mate_titu" value="<?php echo $fila['id_mate']; ?>">
                                        <input type="hidden" name="plaza_titu_asig" value="<?php echo $fila['plaza_titu']; ?>">

                                        <!--<input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">-->
                                        <input type="submit" name="accion" value="Borrar" class="btn btn-danger">


                                    </form>
                                </div>
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#ciclo_titu_asig').change(function() {
            recargarlista();
        });

    });
</script>

<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/asig_titusel.php",
            data: "ciclo=" + $('#ciclo_titu_asig').val(),
            success: function(r) {
                $('#depe_titu_asig').html(r);
            }
        })

        $.ajax({
            type: "POST",
            url: "./selects/asig_evasel.php",
            data: "depe=" + $('#depe_eva_asig').val(),
            success: function(r) {
                $('#plaza_eva_asig').html(r);
            }
        })

    }
</script>

<?php include('../templates/pie.php'); ?>