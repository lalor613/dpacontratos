<?php include("../templates/cabecera.php"); ?>
<?php require '../config/bd.php';


if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $errores = '';
    
    $plaza_titu = filter_var($_POST['plaza_titu'], FILTER_SANITIZE_NUMBER_INT);
    $nombre_titu = filter_var(strtoupper($_POST['nombre_titu']), FILTER_SANITIZE_STRING);
    $tipo_permiso =filter_var(strtoupper($_POST['tipo_permiso']), FILTER_SANITIZE_STRING);
    $motivo_per = filter_var(strtoupper($_POST['motivo_per']), FILTER_SANITIZE_STRING);
    $titu_depe = (isset($_POST['titu_depe'])) ? $_POST['titu_depe'] : "";
    $titu_ciclo = (isset($_POST['titu_ciclo'])) ? $_POST['titu_ciclo'] : "";
    
    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    if(empty($plaza_titu) or empty($nombre_titu) or empty($tipo_permiso) or empty($motivo_per)){
        $errores .= "<li> Llena todos los campos </li>";
    }else{
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        
        switch ($accion) {
            case 'Aceptar':

                $insertar = pg_prepare($conn, "insert_titu" ,"INSERT INTO titulares VALUES (\$1, \$2, \$3, \$4, \$5, \$6)");

                if ($plaza_titu AND $titu_ciclo){
                    $vertitus = pg_prepare($conn, "exist_titu", "SELECT * FROM titulares WHERE plaza_titu = \$1 AND titu_ciclo = \$2 ");
                    $vertitus = pg_execute($conn, "exist_titu", array($plaza_titu, $titu_ciclo ));
                    $resultado = pg_fetch_assoc($vertitus);
                    if ($resultado == False){
                        $insertar = pg_execute($conn, "insert_titu", array( $plaza_titu , $nombre_titu , $tipo_permiso , $motivo_per , $titu_depe , $titu_ciclo ));
                        $errores.= "<li> Titular agregado </li>";
                    }else{
                        $errores .= "<li> El titular ya existe en el ciclo seleccionado: ". $titu_ciclo  ."</li>";
                    }
                }

                break;
                /* case 'Modificar':
                unset($_POST['accion']);
                $modificar = pg_query($conn, "UPDATE titulares SET plaza_titu='$plaza_titu', nombre_titu='$nombre_titu', tipo_permiso='$tipo_permiso', motivo_per='$motivo_per', titu_depe='$titu_depe', titu_ciclo='$titu_ciclo' ");
                break;*/
            case 'Cancelar':
                header("Location: titulares.php");
                break;
            case 'Seleccionar':
                $seleccionar = pg_query($conn, "SELECT * FROM titulares WHERE plaza_titu='$plaza_titu' ");
                break;
            case 'Borrar':
                $borrar = pg_prepare($conn, "del_titu", "DELETE FROM titulares WHERE plaza_titu=  \$1 AND titu_ciclo =  \$2 ");
                $borrar = pg_execute($conn, "del_titu" , array($plaza_titu, $titu_ciclo));
                break;
        }

    }

}

$mostrar = pg_query($conn, "SELECT * FROM view_titulares_dependencias");
if (!$mostrar) {
    echo 'Ocurrió un error\n';
    exit;
}

$depeciclo_view = pg_query($conn, "SELECT * FROM view_dependencia_cicloesc");

?>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md-1-5" data-toggle="modal" data-target="#modelId">
    AGREGAR A DOCENTE TITULAR
</button>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DOCENTE TITULAR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="col form-group">
                                <label for="titu_ciclo">Ciclo escolar</label>
                                <select class="form-control" name="titu_ciclo" id="titu_ciclo">
                                    <option value="0">--Elige ciclo escolar--</option>
                                    <option value="20221">CICLO ESCOLAR 1 ENERO AL 31 DE JULIO 2021</option>
                                    <option value="20222">CICLO ESCOLAR 1 DE AGOSTO AL 31 DE DICIEMBRE 2022</option>
                                </select>
                            </div>
                            <div class="col form-group" id="titu_depe"> </div>
                        </div>

                        <div class="form-row">
                            <div class="col-sm-5">
                                <label for="plaza_titu">Plaza</label>
                                <input type="number" required value="" name="plaza_titu" id="plaza_titu" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombre_titu">Nombre completo</label>
                            <input type="text" required value="" name="nombre_titu" id="nombre_titu" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>

                        <div class="form-group">
                            <label for="tipo_permiso">Permiso</label>
                            <input type="text" required name="tipo_permiso" value="" id="tipo_permiso" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>

                        <div class="form-group">
                            <label for="motivo_per">Motivo</label>
                            <input type="text" required name="motivo_per" value="" id="motivo_per" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>




                        <br>
                        <div class="btn-group responsive" role="group">
                            <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Aceptar</button>
                            <button type="submit" name="accion" value="Cancelar" class="btn btn-info">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
    <div class="col-md-7  ">
        <table class="table table-striped mb-0 table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Plaza</th>
                    <th>Nombre</th>
                    <th>Permiso</th>
                    <th>Dependencia</th>
                    <th>Ciclo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($fila = pg_fetch_assoc($mostrar)) { ?>
                    <tr>
                        <td scope="row"><?php echo $fila['plaza_titu']; ?></td>
                        <td><?php echo $fila['nombre_titu']; ?></td>
                        <td><?php echo $fila['tipo_permiso']; ?></td>
                        <td><?php echo $fila['nombre_depe']; ?></td>
                        <td><?php echo $fila['titu_ciclo']; ?></td>
                        <td>

                            <div class="form-group">
                                <form method="POST">
                                    <input type="hidden" name="plaza_titu" value="<?php echo $fila['plaza_titu']; ?>">
                                    <input type="hidden" name="nombre_titu" value="<?php echo $fila['nombre_titu']; ?>">
                                    <input type="hidden" name="tipo_permiso" value="<?php echo $fila['tipo_permiso']; ?>">
                                    <input type="hidden" name="motivo_per" value="<?php echo $fila['motivo_per']; ?>">
                                    <input type="hidden" name="titu_depe" value="<?php echo $fila['titu_depe']; ?>">
                                    <input type="hidden" name="titu_ciclo" value="<?php echo $fila['titu_ciclo']; ?>">

                                    <input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">
                                    <input type="submit" name="accion" value="Borrar" class="btn btn-danger">
                                </form>
                            </div>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        recargarlista();

        $('#titu_ciclo').change(function(){
            recargarlista();
        });
    });
</script>

<script type="text/javascript">
    function recargarlista(){
        $.ajax({
            type:"POST",
            url:"./selects/dependencias_titusel.php",
            data:"ciclo=" + $('#titu_ciclo').val(),
            success:function(r){
                $('#titu_depe').html(r);
            }
        })
    }
</script>

<?php include("../templates/pie.php"); ?>