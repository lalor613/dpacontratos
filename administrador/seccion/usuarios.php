<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';
    $nombre = filter_var(strtolower($_POST['nombre']), FILTER_SANITIZE_STRING);

    $correo = filter_var($_POST['correo'], FILTER_SANITIZE_EMAIL);


    $contraseña = (isset($_POST['contraseña'])) ? $_POST['contraseña'] : "";
    $tipo_id = (isset($_POST['tipo_id'])) ? $_POST['tipo_id'] : "";
    $depe_user = (isset($_POST['depe_user'])) ? $_POST['depe_user'] : "null";
    $user_ciclo = (isset($_POST['user_ciclo'])) ? $_POST['user_ciclo'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    if (empty($nombre) or empty($correo) or empty($contraseña)) {
        $errores .= "<li> Llena todos los campos </li>";
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }

        switch ($accion) {
            case 'Aceptar':

                $insertar = pg_prepare($conn, "insertar_usuario", "INSERT INTO usuarios (nombre, contraseña, correo, tipo_id, depe_user, user_ciclo) 
                VALUES ($1, $2, $3, $4, $5, $6) ");

                if (filter_var($correo, FILTER_VALIDATE_EMAIL) AND $user_ciclo) {
                    $vercorreo = pg_prepare($conn, "email_exist", "SELECT * FROM usuarios where correo = \$1 AND user_ciclo = \$2");
                    $vercorreo = pg_execute($conn, "email_exist", array($correo, $user_ciclo));
                    $resultado = pg_fetch_assoc($vercorreo);
                    if ($resultado == False) {
                        $contraseña = hash('sha512', $contraseña);
                        $insertar = pg_execute($conn, "insertar_usuario", array($nombre, $contraseña, $correo, $tipo_id, $depe_user, $user_ciclo));
                        $errores .= "<li> Direccion de correo válida </i>";
                    } else {
                        $errores .= "<li> El correo ya fue ingresado en el ciclo selecionado: " . $user_ciclo . "</li>";
                    }
                } else {
                    $errores .= "<li> Ingrese un correo válido </i>";
                }
                break;
            case 'Cancelar':
                header('Location: usuarios.php');
                break;
        }
    }
}

$tipouser = pg_query($conn, "SELECT * FROM tipo_user");

?>

<div class="col-md-6">
    <div class="container-fluid">

        <div class="card">
            <div class="card-header">
                Usuario
            </div>
            <div class="card-body">
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="POST">

                    <div class="form-row">
                        <div class="col form-group">
                            <label for="user_ciclo">Ciclo escolar</label>
                            <select class="form-control" name="user_ciclo" id="user_ciclo">
                                <option value="0">--Elige ciclo escolar--</option>
                                <option value="20221">CICLO ESCOLAR 1 ENERO AL 31 DE JULIO 2021</option>
                                <option value="20222">CICLO ESCOLAR 1 DE AGOSTO AL 31 DE DICIEMBRE 2022</option>
                            </select>
                        </div>
                        <div class="col form-group" id="depe_user"> </div>
                    </div>

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingresa nombre del usuario">
                    </div>

                    <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="email" name="correo" id="correo" class="form-control" placeholder="@unach.mx">
                    </div>

                    <div class="form-group">
                        <label for="contraseña">Contraseña</label>
                        <input type="text" name="contraseña" id="contraseña" class="form-control" placeholder="Ingresa contraseña provicional">
                    </div>

                    <div class="form-group">
                        <label for="tipo_id">Selecciona el tipo de usuario</label>
                        <select class="form-control" name="tipo_id" id="tipo_id">
                            <option></option>
                            <?php while ($fila = pg_fetch_array($tipouser)) {
                                echo "<option value=" . $fila['id'] . ">" . $fila['nombre_tipo'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>



                    <?php if (!empty($errores)) : ?>
                        <p class="form-text text-muted">
                            <?php echo $errores; ?>
                        </p>
                    <?php endif; ?>

                    <div class="btn-group responsive" role="group">
                        <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Aceptar</button>
                        <button type="submit" name="accion" value="Cancelar" class="btn btn-info" >Cancelar</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="col-md-9">

</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#user_ciclo').change(function() {
            recargarlista();
        })
    })
</script>

<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/dependencias_usuarios.php",
            data: "ciclo=" + $('#user_ciclo').val(),
            success: function(r) {
                $('#depe_user').html(r);
            }
        })
    }
</script>


<?php include('../templates/pie.php'); ?>