<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $plaza_eva = filter_var($_POST['plaza_eva'], FILTER_SANITIZE_NUMBER_INT);
    $nombre_eva = filter_var(strtoupper($_POST['nombre_eva']), FILTER_SANITIZE_STRING);
    $edad = filter_var($_POST['edad'], FILTER_SANITIZE_NUMBER_INT);
    $curp =  filter_var(strtoupper($_POST['curp']), FILTER_SANITIZE_STRING);

    $nacionalidad = filter_var(strtoupper($_POST['nacionalidad']), FILTER_SANITIZE_STRING);
    $ciudad = filter_var(strtoupper($_POST['ciudad']), FILTER_SANITIZE_STRING);
    $colonia = filter_var(strtoupper($_POST['colonia']), FILTER_SANITIZE_STRING);
    $cp = filter_var($_POST['cp'], FILTER_SANITIZE_NUMBER_INT);

    $titulo = filter_var(strtoupper($_POST['titulo']), FILTER_SANITIZE_STRING);
    $uni_titulo = filter_var(strtoupper($_POST['uni_titulo']), FILTER_SANITIZE_STRING);
    $fecha_titulo = (isset($_POST['fecha_titulo'])) ? $_POST['fecha_titulo'] : "";
    $cedula = filter_var($_POST['cedula'], FILTER_SANITIZE_NUMBER_INT);

    $periodo_ini = (isset($_POST['periodo_ini'])) ? $_POST['periodo_ini'] : "";
    $periodo_fin = (isset($_POST['periodo_fin'])) ? $_POST['periodo_fin'] : "";

    $eva_depe = (isset($_POST['eva_depe'])) ? $_POST['eva_depe'] : "";
    $eva_ciclo = (isset($_POST['eva_ciclo'])) ? $_POST['eva_ciclo'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";


    if (empty($plaza_eva) or empty($nombre_eva) or empty($curp) or empty($nacionalidad) or empty($ciudad) or empty($colonia) or empty($cp) or empty($titulo)) {
        $errores .= "<li> Llena todos los campos </li>";
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }

        switch ($accion) {

            case 'Aceptar':
                unset($_POST['accion']);
                $insertar = pg_prepare($conn, "insert_eva", "INSERT INTO evaluados VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16 )");

                if ($curp and $eva_ciclo) {
                    $verevas = pg_prepare($conn, "eva_exist", "SELECT * FROM evaluados WHERE curp = \$1 AND eva_ciclo = \$2 ");
                    $verevas = pg_execute($conn, "eva_exist", array($curp, $eva_ciclo));
                    $resultado = pg_fetch_assoc($verevas);
                    if ($resultado == False) {
                        $insertar = pg_execute($conn, "insert_eva", array($plaza_eva, $nombre_eva, $edad, $curp, $nacionalidad, $ciudad, $colonia, $cp, $titulo, $uni_titulo, $fecha_titulo, $cedula, $periodo_ini, $periodo_fin, $eva_depe, $eva_ciclo));
                        $errores .= "<li> Docente agregado </li>";
                    } else {
                        $errores .= "<li> El docente ya existe </li>";
                    }
                }

                break;

            case 'Modificar':
                unset($_POST['accion']);
                $modificar = pg_query($conn, "UPDATE evaluados SET 
                plaza_eva='$plaza_eva', nombre_eva ='$nombre_eva', edad = '$edad', curp ='$curp', nacionalidad = '$nacionalidad', ciudad = '$ciudad', colonia = '$ciudad', cp = '$cp' , titulo = '$titulo', uni_titulo = '$uni_titulo',
                fecha_titulo = '$fecha_titulo', cedula = '$cedula' , periodo_ini='$periodo_ini', periodo_fin='$periodo_fin', eva_depe = '$eva_depe', eva_ciclo = '$eva_ciclo' ");
                break;

            case 'Cancelar':
                header("Location: evaluados.php");
                break;

            case 'Seleccionar':
                $seleccionar = pg_query($conn, "SELECT * FROM evaluados WHERE plaza_eva='$plaza_eva' ");
                break;

            case 'Borrar':
                $borrar = pg_prepare($conn, "del_eva", "DELETE FROM evaluados WHERE plaza_eva=  \$1 AND eva_ciclo =  \$2 ");
                $borrar = pg_execute($conn, "del_eva", array($plaza_eva, $eva_ciclo));
                break;
        }
    }
}

$mostrar = pg_query($conn, "SELECT * FROM view_evaluados_dependencias");
if (!$mostrar) {
    echo 'Ocurrio un error\n';
    exit;
}

$depeciclo_view = pg_query($conn, "SELECT * FROM view_dependencia_cicloesc");
?>

<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md-1-5" data-toggle="modal" data-target="#modelId">
    AGREGAR DOCENTE
</button>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DOCENTE A CONTRATAR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="col form-group">
                                <label for="eva_ciclo">Ciclo escolar</label>
                                <select class="form-control" name="eva_ciclo" id="eva_ciclo">
                                    <option value="0">--Elige ciclo escolar--</option>
                                    <option value="20221">CICLO ESCOLAR 1 ENERO AL 31 DE JULIO 2021</option>
                                    <option value="20222">CICLO ESCOLAR 1 DE AGOSTO AL 31 DE DICIEMBRE 2022</option>
                                </select>
                            </div>
                            <div class="col form-group" id="eva_depe"></div>
                        </div>


                        <h1-6><span class="badge badge-primary">Periodo laboral</span></h1-6>
                        <div class="form-row">
                            <div class="col">
                                <label for="periodo_ini">Inicio</label>
                                <input type="date" name="periodo_ini" id="periodo_ini" class="form-control">
                            </div>
                            <div class="col">
                                <label for="periodo_fn">Fin</label>
                                <input type="date" name="periodo_fin" id="periodo_fin" class="form-control">
                            </div>
                        </div>
                        <h1-6><span class="badge badge-primary">Datos personales</span></h1-6>
                        <div class="form-group">
                            <label for="nombre_eva">Nombre completo</label>
                            <input type="text" name="nombre_eva" id="nombre_eva" class="form-control" placeholder="Ingresa el nombre" aria-describedby="helpId">
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="edad">Edad</label>
                                <input type="number" name="edad" id="edad" class="form-control">
                            </div>

                            <div class="col-md-6">
                                <label for="curp">Curp</label>
                                <input type="text" name="curp" id="curp" class="form-control">
                            </div>

                            <div class="col ">
                                <label for="plaza_eva">Plaza UNACH</label>
                                <input type="number" name="plaza_eva" id="plaza_eva" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ciudad">Ciudad</label>
                            <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="" aria-describedby="helpId">
                            <small id="helpId" class="text-muted">Debe coincidir con el comp. domicilio</small>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="colonia">Colonia</label>
                                <input type="text" name="colonia" id="colonia" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                            <div class="col">
                                <label for="cp">Código postal</label>
                                <input type="number" name="cp" id="cp" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                            <div class="col">
                                <label for="nacionalidad">Nacionalidad</label>
                                <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                        </div>
                        <h1-6><span class="badge badge-primary">Datos académicos</span></h1-6>
                        <div class="form-group">
                            <label for="uni_titulo">Universidad</label>
                            <input type="text" name="uni_titulo" id="uni_titulo" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>

                        <div class="form-group">
                            <label for="titulo">Perfil</label>
                            <input type="text" name="titulo" id="titulo" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="cedula">Cédula</label>
                                <input type="number" name="cedula" id="cedula" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                            <div class="col">
                                <label for=fecha_titulo">Fecha de obtención</label>
                                <input type="date" name="fecha_titulo" id="fecha_titulo" class="form-control">
                            </div>
                        </div>


                        <br>

                        <div class="btn_group" role="group">
                            <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Guardar</button>
                            <button type="submit" name="accion" value="Cancelar" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>


<div class="row justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
    <div class="col-sm-8">
        <table class="table table-bordered table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Plaza</th>
                    <th>Nombre</th>
                    <th>Titulo</th>
                    <th>Periodo Inicio</th>
                    <th>Periodo Fin</th>
                    <th>Dependencia</th>
                    <th>Ciclo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($fila = pg_fetch_assoc($mostrar)) {
                ?>

                    <tr>
                        <td scope="row"><?php echo $fila['plaza_eva']; ?></td>
                        <td><?php echo $fila['nombre_eva']; ?></td>
                        <td><?php echo $fila['uni_titulo']; ?></td>
                        <td class="text-nowrap"><?php echo $fila['periodo_ini']; ?></td>
                        <td class="text-nowrap"><?php echo $fila['periodo_fin']; ?></td>
                        <td><?php echo $fila['nombre_depe']; ?></td>
                        <td><?php echo $fila['eva_ciclo']; ?></td>
                        <td>
                            <div class="form-group">
                                <form method="POST">
                                    <input type="hidden" name="plaza_eva" value="<?php echo $fila['plaza_eva']; ?>">
                                    <input type="hidden" name="nombre_eva" value="<?php echo $fila['nombre_eva']; ?>">
                                    <input type="hidden" name="uni_titulo" value="<?php echo $fila['uni_titulo']; ?>">
                                    <input type="hidden" name="periodo_ini" value="<?php echo $fila['periodo_ini']; ?>">
                                    <input type="hidden" name="periodo_fin" value="<?php echo $fila['periodo_fin']; ?>">
                                    <input type="hidden" name="eva_ciclo" value="<?php echo $fila['eva_ciclo']; ?>">
                                    <input type="hidden" name="eva_depe" value="<?php echo $fila['eva_depe']; ?>">

                                    <input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">
                                    <input type="submit" name="accion" value="Borrar" class="btn btn-danger">
                                </form>
                            </div>
                        </td>
                    </tr>

                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#eva_ciclo').change(function() {
            recargarlista();
        });
    });
</script>

<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/dependencias_evasel.php",
            data: "ciclo=" + $('#eva_ciclo').val(),
            success: function(r) {
                $('#eva_depe').html(r);
            }
        })
    }
</script>

<?php include('../templates/pie.php'); ?>