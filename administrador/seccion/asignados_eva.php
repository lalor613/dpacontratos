<?php include('../templates/cabecera.php');  ?>
<?php require '../config/bd.php';



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $plaza_eva_asig = (isset($_POST['plaza_eva_asig'])) ? $_POST['plaza_eva_asig'] : "";
    $depe_eva_asig = (isset($_POST['depe_eva_asig'])) ? $_POST['depe_eva_asig'] : "";
    $ciclo_eva_asig = (isset($_POST['ciclo_eva_asig'])) ? $_POST['ciclo_eva_asig'] : "";
    $id_mate_eva = (isset($_POST['id_mate_eva'])) ? $_POST['id_mate_eva'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    switch ($accion) {
        case 'Agregar':
            $insertar = pg_prepare($conn, "insert_asigeva", "INSERT INTO asignados_eva VALUES ( \$1, \$2, \$3, \$4 )");

            if ($plaza_eva_asig and $id_mate_eva) {
                $verevaasig = pg_prepare($conn, "evaasig_exist", "SELECT * FROM asignados_eva WHERE plaza_eva_asig = \$1 AND id_mate_eva = \$2");
                $verevaasig = pg_execute($conn, "evaasig_exist", array($plaza_eva_asig, $id_mate_eva));
                $resultado = pg_fetch_assoc($verevaasig);
                if ($resultado == False) {
                    $insertar = pg_execute($conn, "insert_asigeva", array($plaza_eva_asig, $depe_eva_asig, $ciclo_eva_asig, $id_mate_eva));
                    $errores .= "<li> Asignatura cargada </li>";
                } else {
                    $errores .= "<li> La materia ya fue asignada a este maestro </li>";
                }
            }
            break;

        case 'Borrar':
            $insertar = pg_prepare($conn, "del_asigeva", "DELETE FROM asignados_eva WHERE plaza_eva_asig =\$1 AND depe_eva_asig = \$2 AND ciclo_eva_asig = \$3 AND id_mate_eva = \$4 ");
            $insertar = pg_execute($conn, "del_asigeva", array($plaza_eva_asig, $depe_eva_asig, $ciclo_eva_asig, $id_mate_eva));
            break;
    }
}

$captardepeeva = pg_query($conn, "SELECT * FROM view_evaluados_dependencias");
$captarciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");

$materias = pg_query($conn, "SELECT * FROM materias");
$mostraasig = pg_query($conn, "SELECT * FROM view_asignados_eva");

?>
<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>

<div class="col-md-6">
    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

        <div class="card">
            <div class="card-header">
                Elige Docente
            </div>

            <div class="card-body">
                <div class="form-row">

                    <div class="col">
                        <label for="ciclo_eva_asig">Ciclo escolar</label>
                        <select class="form-control" name="ciclo_eva_asig" id="ciclo_eva_asig">
                            <option value=0>--Elige el ciclo--</option>
                            <?php
                            while ($fila = pg_fetch_assoc($captarciclo)) {
                                echo "<option value=" . $fila['id_ciclo'] . ">" . $fila['periodo_des'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col form-group" id="depe_eva_asig"> </div>


                </div>

                <div class="form-group">
                    <label for="plaza_eva_asig">Nombre</label>
                    <select class="form-control" name="plaza_eva_asig" id="plaza_eva_asig">
                        <option selected>--Eliga al docente---</option>
                        <?php
                        while ($fila = pg_fetch_assoc($captardepeeva)) {
                            echo "<option value=" . $fila['plaza_eva'] . ">" . $fila['nombre_eva'] . "</option>";
                        }
                        ?>
                    </select>
                </div>

            </div>

            <div class="card-header">
                Elige materia
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="id_mate_eva">Materia(s)</label>
                    <select name="id_mate_eva" multiple class="form-control">
                        <option selected>-Selecciona una materia-</option>
                        <?php
                        while ($fila = pg_fetch_array($materias)) {
                            echo "<option value=" . $fila['id_mate'] . ">" . $fila['nom_materia'] . "\t|\t" . $fila['semestre'] . "\t|\t" . $fila['grupo'] . "\t|\t" . $fila['hsm'] . "</option>";
                        } ?>
                    </select>
                </div>

                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="submit" name="accion" value="Agregar" class="btn btn-success">Agregar</button>
                </div>
            </div>

        </div>

    </form>
</div>


<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            Materias cargadas
            <small class="text-muted">cargado a: (-fulanito-)</small>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-inverse table-responsive">
                <thead class="thead-inverse text-center">
                    <tr>
                        <th>Docente</th>
                        <th>Asignatura</th>
                        <th>Semestre</th>
                        <th>Grupo</th>
                        <th>Carga horaria</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-nowrap">
                    <?php
                    while ($fila = pg_fetch_assoc($mostraasig)) {
                    ?>
                        <tr>
                            <td>
                                <?php echo $fila['nombre_eva']; ?><br>
                                <small class="text-muted"><?php echo $fila['plaza_eva']; ?></small>
                            </td>
                            <td>
                                <small class="text-muted"><?php echo $fila['plan_est']; ?></small><br>
                                <?php echo 'ID: ' . $fila['id_mate'] . ' > ' . $fila['nom_materia']; ?>
                            </td>
                            <td><?php echo $fila['semestre']; ?></td>
                            <td><?php echo $fila['grupo']; ?></td>
                            <td><?php echo $fila['hsm']; ?> </td>
                            <td>
                                <div class="form-group">
                                    <form method="POST">
                                        <input type="hidden" name="depe_eva_asig" value="<?php echo $fila['depe_eva_asig']; ?>">
                                        <input type="hidden" name="ciclo_eva_asig" value="<?php echo $fila['ciclo_eva_asig']; ?>">
                                        <input type="hidden" name="id_mate_eva" value="<?php echo $fila['id_mate']; ?>">
                                        <input type="hidden" name="plaza_eva_asig" value="<?php echo $fila['plaza_eva']; ?>">

                                        <!--<input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">-->
                                        <input type="submit" name="accion" value="Borrar" class="btn btn-danger">


                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#ciclo_eva_asig').change(function() {
            recargarlista();
        });

    });
</script>

<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/evajs/asig_evasel.php",
            data: "ciclo=" + $('#ciclo_eva_asig').val(),
            success: function(r) {
                $('#depe_eva_asig').html(r);
            }
        })

        $.ajax({
            type: "POST",
            url: "./selects/asig_evasel.php",
            data: "depe=" + $('#depe_eva_asig').val(),
            success: function(r) {
                $('#plaza_eva_asig').html(r);
            }
        })

    }
</script>

<script src="../JS/asigmate.js"></script>

<?php include('../templates/pie.php'); ?>