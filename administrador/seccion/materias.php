<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $plan_est = filter_var( strtoupper($_POST['plan_est']) , FILTER_SANITIZE_STRING);
    $nom_materia = filter_var(strtoupper($_POST['nom_materia']) , FILTER_SANITIZE_STRING);
    $semestre = filter_var($_POST['semestre'], FILTER_SANITIZE_NUMBER_INT);
    $grupo = filter_var(strtoupper($_POST['grupo']), FILTER_SANITIZE_STRING);
    $hsm = filter_var($_POST['hsm'], FILTER_SANITIZE_NUMBER_FLOAT);
    $mate_ciclo = (isset($_POST['mate_ciclo'])) ? $_POST['mate_ciclo'] : "";
    $mate_depe = (isset($_POST['mate_depe'])) ? $_POST['mate_depe'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    if (empty($plan_est) or empty($nom_materia) or empty($semestre) or empty($hsm)) {
        $errores = "<li> Llena todos los campos </li>";
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }

        switch ($accion) {

            case 'Aceptar':

                $insertar = pg_prepare($conn, "insert_mate", "INSERT INTO materias (plan_est, nom_materia, semestre, grupo, hsm, mate_ciclo, mate_depe) VALUES(\$1,\$2,\$3,\$4,\$5,\$6,\$7) ");
                $insertar = pg_execute($conn, "insert_mate" , array($plan_est,$nom_materia, $semestre, $grupo, $hsm ,$mate_ciclo,$mate_depe));
               
                $errores .= "<li> Materia creada </li>";
                break;

            case 'Modificar':
                $modificar = pg_query($conn, "UPDATE materias SET plan_est='$plan_est', nom_materia='$nom_materia', semestre='$semestre', grupo='$semestre', hsm='$hsm' WHERE id_mate IN '$id_mate' ");
                header('Location:materias.php');
                break;

            case 'Cancelar':
                header("Loacation:materias.php");
                break;

            case 'Seleccionar':
                $seleccionar = pg_query($conn, "SELECT plan_est, nom_materia, semestre, grupo, hsm FROM materias WHERE id_mate='$id_mate' ");
                break;

            case 'Borrar':
                $borrar = pg_prepare($conn, "del_materia", "DELETE FROM materias WHERE plan_est = \$1 AND nom_materia = \$2 AND semestre = \$3 AND grupo = \$4 ");
                $borrar = pg_execute($conn, "del_materia", array($plan_est, $nom_materia, $semestre, $grupo));
               
                $errores .= "<li> Materia borrada </li>";
                break;
        }
    }
}

//$id_mate = (isset($_POST['id_mate'])) ? $_POST['id_mate'] : "";


$mostrar = pg_query($conn, "SELECT id_mate, plan_est ,nom_materia, semestre, grupo, hsm FROM materias ");

$depe = pg_query($conn, "SELECT * FROM dependencias");
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");

?>

<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>

<div class="col-md-5">
    <div class="card">
        <div class="card-header">
            Datos Materias
        </div>
        <div class="card-body">
            <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

                <!--  <div class="form-group">
                  <label for="id_mate">ID materia</label>
                  <input type="number" name="id_mate" id="id_mate" class="form-control" placeholder="" aria-describedby="helpId">
                  <small id="helpId" class="text-muted">Help text</small>
                </div> -->
                <div class="form-row">

                    <div class="col form-group">
                        <label for="mate_ciclo">Ciclo escolar</label>
                        <select class="form-control" name="mate_ciclo" id="mate_ciclo">
                            <option value="0">--Elige ciclo escolar--</option>
                            <option value="20221">CICLO ESCOLAR 1 ENERO AL 31 DE JULIO 2021</option>
                            <option value="20222">CICLO ESCOLAR 1 DE AGOSTO AL 31 DE DICIEMBRE 2022</option>
                        </select>
                    </div>
                    <div class="col form-group" id="mate_depe"></div>

                </div>

                <div class="form-group">
                    <label for="plan_est">Plan de estudios</label>
                    <input type="text" value="" name="plan_est" id="plan_est" class="form-control" placeholder="Licenciatura en..." aria-describedby="helpId">
                </div>

                <div class="form-group">
                    <label for="nom_materia">Asignatura</label>
                    <input type="text" value="" name="nom_materia" id="nom_materia" class="form-control" placeholder="">
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="semestre">Semestre</label>
                        <input type="number" value="" min="1" name="semestre" id="semestre" class="form-control" placeholder="">
                    </div>

                    <div class="col">
                        <label for="grupo">grupo</label>
                        <input type="text" value="" name="grupo" id="grupo" class="form-control" placeholder="">
                    </div>

                    <div class="col">
                        <label for="hsm">Carga horaria</label>
                        <input type="number" value="" min="1" max="40" step="any" name="hsm" id="" class="form-control" placeholder="">
                    </div>

                </div>
                <br>
                <div class="btn-group responsive" role="group" aria-label="Basic example">
                    <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Aceptar</button>
                    <button type="submit" name="accion" value="Modificar" class="btn btn-warning">Modificar</button>
                    <button type="submit" name="accion" value="Cancelar" class="btn btn-info">Cancelar</button>
                </div>

            </form>
        </div>

    </div>
</div>

<div class="col-md-7">
    <table class="table table-bordered table-inverse table-responsive">
        <thead class="thead-inverse">
            <tr>
                <th>No.a</th>
                <th>Asignatura</th>
                <th>Semestre</th>
                <th>Grupo</th>
                <th>Carga horaria</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            <?php while ($fila = pg_fetch_assoc($mostrar)) { ?>
                <tr>
                    <td> <?php echo $fila['id_mate']; ?></td>

                    <td>
                        <small class="text-muted blockquote-footer">
                            <?php echo $fila['plan_est'] ?>
                        </small>
                        <?php echo $fila['nom_materia']; ?>
                    </td>

                    <td> <?php echo $fila['semestre']; ?> </td>
                    <td> <?php echo $fila['grupo']; ?> </td>
                    <td> <?php echo $fila['hsm']; ?> </td>
                    <td>
                        <div class="form-group">
                            <form method="POST">
                                <input type="hidden" name="id_mate" value="<?php echo $fila['id_mate']; ?>">
                                <input type="hidden" name="plan_est" value="<?php echo $fila['plan_est']; ?>">
                                <input type="hidden" name="nom_materia" value="<?php echo $fila['nom_materia']; ?>">
                                <input type="hidden" name="semestre" value="<?php echo $fila['semestre']; ?>">
                                <input type="hidden" name="grupo" value="<?php echo $fila['grupo']; ?>">
                                <input type="hidden" name="hsm" value="<?php echo $fila['hsm']; ?>">

                                <input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">
                                <input type="submit" name="accion" value="Borrar" class="btn btn-danger">

                            </form>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        recargarlista();

        $('#mate_ciclo').change(function() {
            recargarlista();
        });
    });
</script>

<script type="text/javascript">
    function recargarlista() {
        $.ajax({
            type: "POST",
            url: "./selects/dependencias_matesel.php",
            data: "ciclo=" + $('#mate_ciclo').val(),
            success: function(r) {
                $('#mate_depe').html(r);
            }
        })
    }
</script>


<?php include('../templates/pie.php'); ?>