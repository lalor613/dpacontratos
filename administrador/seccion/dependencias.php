<?php include('../templates/cabecera.php'); ?>
<?php require '../config/bd.php';


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $clave_depe = filter_var($_POST['clave_depe'], FILTER_SANITIZE_NUMBER_INT);
    $depe_ciclo = filter_var($_POST['depe_ciclo'], FILTER_SANITIZE_NUMBER_INT);
    $nombre_depe = (isset($_POST['nombre_depe'])) ? $_POST['nombre_depe'] : "";
    $directivo_nom = filter_var(strtoupper($_POST['directivo_nom']), FILTER_SANITIZE_STRING);
    $puesto = (isset($_POST['puesto'])) ? $_POST['puesto'] : "";
    $fecha_nom = (isset($_POST['fecha_nom'])) ? $_POST['fecha_nom'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    if (empty($clave_depe) or empty($depe_ciclo) or empty($directivo_nom) or empty($fecha_nom)) {
        $errores = "<li> Llena todos los campos </li>";
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }

        switch ($accion) {

            case 'Aceptar':
                $insertar = pg_prepare($conn, "insert_depe", "INSERT INTO dependencias VALUES ($1,$2,$3,$4,$5,$6)");

                if ($clave_depe and $depe_ciclo) {
                    $verdepe = pg_prepare($conn, "depe_exist", "SELECT * FROM dependencias where clave_depe = \$1 AND depe_ciclo = \$2");
                    $verdepe = pg_execute($conn, "depe_exist", array($clave_depe, $depe_ciclo));
                    $resultado = pg_fetch_assoc($verdepe);
                    if ($resultado == False) {
                        $insertar = pg_execute($conn, "insert_depe", array($clave_depe, $nombre_depe, $directivo_nom, $puesto, $fecha_nom, $depe_ciclo));
                        $errores .= "<li> Dependencia creada </li>";
                    } else {
                        $errores .= "<li> La dependencia ya existe en el ciclo seleccionado</li>";
                    }
                }

                break;

            case 'Modificar':
                unset($_POST['accion']);
                $modificar = pg_query($conn, "UPDATE dependencias SET nombre_depe='$nombre_depe', directivo_nom='$directivo_nom', puesto='$puesto', fecha_nom='$fecha_nom', depe_ciclo='$depe_ciclo' WHERE clave_depe = '$clave_depe' ");
                if ($modificar) {
                    header("Location: dependencias.php");
                    echo "Datos Actualizados: $modificar\n";
                }
                break;

            case 'Cancelar':
                header('Location:dependencias.php');
                break;

            case 'Seleccionar':
                $seleccionar = pg_query($conn, "SELECT nombre_depe, directivo_nom, puesto, fecha_nom, depe_ciclo FROM dependencias WHERE clave_depe='$clave_depe' ");
                break;


            case 'Borrar':                

                $borrar = pg_prepare($conn, "borrar_depe", "DELETE FROM dependencias WHERE clave_depe = \$1 AND depe_ciclo= \$2 ");
                $borrar = pg_execute($conn, "borrar_depe", array($clave_depe, $depe_ciclo));
                break;
                
        }
    }
}
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
$exist = pg_num_rows($ciclo);
$seleccionar = pg_query($conn, "SELECT * FROM view_dependencia_cicloesc");
?>
<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md-1-5" data-toggle="modal" data-target="#modaldepe">
    AÑADIR DEPENDENCIA
</button>

<!-- Modal -->
<div class="modal fade" id="modaldepe" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DEPENDENCIA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form method="POST" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">

                        <div class="form-row">

                            <div class="col-md-4">
                                <label for="clave_depe">Clave</label>
                                <input type="number" required min="1" name="clave_depe" value="" id="clave_depe" class="form-control" placeholder="" aria-describedby="helpId">
                            </div>

                            <div class="col">

                                <?php
                                if ($exist > 0) {
                                ?>

                                    <div class="form-group">
                                        <label for="depe_ciclo">Ciclo</label>
                                        <select class="form-control" name="depe_ciclo" required value="" id="depe_ciclo">
                                            <option selected>--Elige un ciclo--</option>
                                            <?php
                                            while ($fila = pg_fetch_array($ciclo)) {
                                                echo "<option value = " . $fila['id_ciclo'] . ">" . $fila['periodo_des'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombre_depe">DEPENDENCIA</label>
                            <select class="form-control" required value="" name="nombre_depe" id="nombre_depe">
                                <option selected>--Elegir dependencia--</option>
                                <option>ESCUELA DE CIENCIAS ADMINISTRATIVAS ISTMO-COSTA, CAMPUS IX </option>
                                <option>ESCUELA DE CIENCIAS ADMINISTRATIVAS, CAMPUS IX </option>
                                <option>ESCUELA DE CIENCIAS QUIMICAS </option>
                                <option>ESCUELA DE CIENCIAS Y PROCESOS AGROPECUARIOS INDUSTRIALES, ISTMO-COSTA, CAMPUS IX </option>
                                <option>ESCUELA DE CONTADURIA Y ADMINISTRACION, CAMPUS VII </option>
                                <option>ESCUELA DE ESTUDIOS AGROPECUARIOS MEZCALAPA </option>
                                <option>ESCUELA DE GESTIÓN Y AUTODESARROLLO INDIGENA </option>
                                <option>ESCUELA DE HUMANIDADES, CAMPUS IV </option>
                                <option>ESCUELA DE HUMANIDADES, CAMPUS IX </option>
                                <option>ESCUELA DE LENGUAS, CAMPUS SAN CRISTOBAL DE LAS CASAS </option>
                                <option>ESCUELA DE LENGUAS, CAMPUS TAPACHULA </option>
                                <option>FACULTAD DE ARQUITECTURA, CAMPUS I </option>
                                <option>FACULTAD DE CIENCIAS ADMINISTRATIVAS, CAMPUS VIII </option>
                                <option>FACULTAD DE CIENCIAS AGRICOLAS CAMPUS IV </option>
                                <option>FACULTAD DE CIENCIAS AGRONOMICAS, CAMPUS V </option>
                                <option>FACULTAD DE CIENCIAS DE LA ADMINISTRACION, CAMPUS IV </option>
                                <option>FACULTAD DE CIENCIAS EN FÍSICA Y MATEMÁTICAS </option>
                                <option>FACULTAD DE CIENCIAS QUIMICAS, CAMPUS IV </option>
                                <option>FACULTAD DE CIENCIAS SOCIALES, CAMPUS III </option>
                                <option>FACULTAD DE CONTADURÍA Y ADMINISTRACION, CAMPUS I </option>
                                <option>FACULTAD DE DERECHO, CAMPUS III </option>
                                <option>FACULTAD DE HUMANIDADES, CAMPUS VI </option>
                                <option>FACULTAD DE INGENIERIA, CAMPUS I </option>
                                <option>FACULTAD DE LENGUAS, CAMPUS TUXTLA </option>
                                <option>FACULTAD DE MEDICINA HUMANA "DR. MANUEL VELASCO SUAREZ", CAMPUS II </option>
                                <option>FACULTAD DE MEDICINA HUMANA "DR. MANUEL VELASCO SUAREZ", CAMPUS IV </option>
                                <option>FACULTAD DE MEDICINA VETERINARIA Y ZOOTECNIA, CAMPUS II </option>
                                <option>FACULTAD DE NEGOCIOS, CAMPUS IV </option>
                                <option>FACULTAD MAYA DE ESTUDIOS AGROPECUARIOS </option>
                                <option>INSTITUTO DE BIOCIENCIAS </option>
                                <option>INSTITUTO DE ESTUDIOS INDIGENAS </option>
                                <option>INSTITUTO DE INVESTIGACIONES JURIDICAS </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="directivo_nom">Nombre del Directivo</label>
                            <input type="text" required name="directivo_nom" value="" id="directivo_nom" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>

                        <div class="form-group">
                            <label for="puesto">Cargo</label>
                            <select class="form-control" required name="puesto" id="puesto">
                                <option selected>--Elegir cargo--</option>
                                <option>COORDINADORA</option>
                                <option>COORDINADOR</option>
                                <option>COORDINADORA ACADEMICA</option>
                                <option>COORDINADOR ACADEMICO</option>
                                <option>COORDINADORA GENERAL</option>
                                <option>COORDINADOR GENERAL</option>
                                <option>DIRECTORA</option>
                                <option>DIRECTOR</option>
                                <option>ENCARGADA DE LA DIRECCION</option>
                                <option>ENCARGADO DE LA DIRECCION</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="fecha_nom">Fecha de nombramiento</label>
                            <input type="date" required name="fecha_nom" value="" id="fecha_nom" class="form-control">
                        </div>

                        <div class="btn_group" role="group">
                            <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Aceptar</button>
                            <button type="submit" name="accion" value="Cancelar" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>
    </div>
</div>



<div class="row justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
    <div class="col-xl-9">

        <table id="dtBasicExample" class="table table-striped table-bordered table-responsive table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Clave</th>
                    <th>Nombre</th>
                    <th>Directivo</th>
                    <th>Puesto</th>
                    <th>Fecha de Nombramiento</th>
                    <th>Ciclo</th>
                    <th>Acciones</th>
                </tr>
            </thead>

            <tbody>
                <?php
                while ($fila = pg_fetch_assoc($seleccionar)) {
                ?>
                    <tr>
                        <td scope="row"><?php echo $fila['clave_depe']; ?></td>
                        <td><?php echo $fila['nombre_depe']; ?></td>
                        <td><?php echo $fila['directivo_nom']; ?></td>
                        <td><?php echo $fila['puesto']; ?></td>
                        <td><?php echo $fila['fecha_nom']; ?></td>
                        <td><?php echo $fila['depe_ciclo']; ?></td>
                        <td>

                            <div class="form-group">
                                <form method="POST">

                                    <input type="hidden" name="clave_depe" value="<?php echo $fila['clave_depe']; ?>">
                                    <input type="hidden" name="nombre_depe" value="<?php echo $fila['nombre_depe']; ?>">
                                    <input type="hidden" name="directivo_nom" value="<?php echo $fila['directivo_nom']; ?>">
                                    <input type="hidden" name="puesto" value="<?php echo $fila['puesto']; ?>">
                                    <input type="hidden" name="fecha_nom" value="<?php echo $fila['fecha_nom']; ?>">
                                    <input type="hidden" name="depe_ciclo" value="<?php echo $fila['depe_ciclo']; ?>">

                                    <input type="button" name="accion" value="Seleccionar" class="btn btn-primary" data-toggle="modal" data-target="#modaldepe">
                                    <input type="submit" name="accion" value="Borrar" class="btn btn-danger">

                                </form>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>



    </div>

</div>








<?php include('../templates/pie.php'); ?>