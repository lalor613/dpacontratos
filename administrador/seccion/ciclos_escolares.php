<?php include("../templates/cabecera.php"); ?>
<?php require "../config/bd.php";


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errores = '';

    $id_ciclo = filter_var($_POST['id_ciclo'], FILTER_SANITIZE_NUMBER_INT);
    $fecha_inicio = (isset($_POST['fecha_inicio'])) ? $_POST['fecha_inicio'] : "";
    $fecha_termino = (isset($_POST['fecha_termino'])) ? $_POST['fecha_termino'] : "";
    $fecha_inicio_cortado = (isset($_POST['fecha_inicio_cortado'])) ? $_POST['fecha_inicio_cortado'] : "";
    $fecha_termino_cortado = (isset($_POST['fecha_termino_cortado'])) ? $_POST['fecha_termino_cortado'] : "";
    $periodo_des = (isset($_POST['periodo_des'])) ? $_POST['periodo_des'] : "";
    $periodo_des_cortado = (isset($_POST['periodo_des_cortado'])) ? $_POST['periodo_des_cortado'] : "";

    $accion = (isset($_POST['accion'])) ? $_POST['accion'] : "";

    if (empty($id_ciclo) or empty($fecha_inicio) or empty($fecha_termino)) {
        $errores .= "<li> Llena todos los campos </li>";
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }

        switch ($accion) {
            case "Aceptar":
                $insertar = pg_prepare($conn, "insert_ciclo", "INSERT INTO ciclos_escolares VALUES ($1,$2,$3,$4,$5,$6,$7)");
                if ($id_ciclo) {
                    $verciclo = pg_prepare($conn, "ciclo_exist", "SELECT * FROM ciclos_escolares where id_ciclo = \$1");
                    $verciclo = pg_execute($conn, "ciclo_exist", array($id_ciclo));
                    $resultado = pg_fetch_assoc($verciclo);
                    if ($resultado == False) {
                        $insertar = pg_execute($conn, "insert_ciclo", array(
                            $id_ciclo, $fecha_inicio, $fecha_termino, $fecha_inicio_cortado, $fecha_termino_cortado, $periodo_des, $periodo_des_cortado
                        ));
                        $errores .= "<li> Ciclo creado </li>";
                    } else {
                        $errores .= "<li> El ciclo ya existe </li>";
                    }
                }

                //echo "Se presionó \"AGREGAR\" ";
                //header("Location:ciclos_escolares.php");
                break;
            case "Cancelar":
                header("Location: ciclos_escolares.php");
                break;
        }
    }
}


//echo $id_ciclo. "<br/>";
//echo $fecha_inicio. "<br/>";
//echo $fecha_termino. "<br/>";
//echo $fecha_inicio_cortado. "<br/>";
//echo $fecha_termino_cortado. "<br/>";
//echo $periodo_des. "<br/>";
//echo $periodo_des_cortado. "<br/>";



//MOSTRAR LOS VALORE DE LA TABLA 1.
$seleccionar = pg_query($conn, "SELECT id_ciclo, fecha_inicio, fecha_termino, fecha_inicio_cortado, fecha_termino_cortado FROM ciclos_escolares");
if (!$seleccionar) {
    echo 'Ocurrió un error';
    exit;
}

?>

<?php if (!empty($errores)) : ?>
    <p class="form-text text-muted">
        <?php echo $errores; ?>
    </p>
<?php endif; ?>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md-1-5" data-toggle="modal" data-target="#modelId">
    AÑADIR CICLO
</button>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">CICLO ESCOLAR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="id_ciclo">ID CICLO ESCOLAR</label>
                            <input type="number" required value="" name="id_ciclo" id="id_ciclo" class="form-control" placeholder="">
                        </div>

                        <div class="form-row">

                            <div class="col">
                                <label for="fecha_inicio">Inicio del ciclo escolar</label>
                                <input type="date" required value="" name="fecha_inicio" id="fecha_inicio" class="form-control" placeholder="">
                            </div>

                            <div class="col">
                                <label for="fecha_termino">Fin del ciclo escolar</label>
                                <input type="date" required value="" name="fecha_termino" id="fecha_termino" class="form-control" placeholder="">
                            </div>

                        </div>
                        <br>
                        <div class="form-row">

                            <div class="col">
                                <label for="fecha_inicio_cortado">Inicio ciclo escolar cortado</label>
                                <input type="date" required value="" name="fecha_inicio_cortado" id="fecha_inicio_cortado" class="form-control" placeholder="">
                            </div>

                            <div class="col">
                                <label for="fecha_termino_cortado">Fin ciclo escolar cortado</label>
                                <input type="date" required value="" name="fecha_termino_cortado" id="fecha_termino_cortado" class="form-control" placeholder="">
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="periodo_des">Descripcion del ciclo escolar</label>
                            <input type="text" required value="" name="periodo_des" id="periodo_des" class="form-control" require>
                        </div>

                        <div class="form-group">
                            <label for="periodo_des_cortado">Descripcion del ciclo escolar cortado</label>
                            <input type="text" required value="" name="periodo_des_cortado" id="periodo_des_cortado" class="form-control" require>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="submit" name="accion" value="Aceptar" class="btn btn-success">Aceptar</button>
                            <button type="submit" name="accion" value="Cancelar" class="btn btn-info" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="row justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
    <div class="col-xl-6">
        <table class="table table-striped table-inverse table-responsive">
            <thead class="thead-inverse text-center">
                <tr>
                    <th>ID</th>
                    <th>Fecha inicio</th>
                    <th>Fecha termino</th>
                    <th>Fecha inicio cortado</th>
                    <th>Fecha termino cortado</th>

                </tr>
            </thead>
            <tbody class="text-nowrap text-center">
                <?php
                while ($fila = pg_fetch_assoc($seleccionar)) {
                ?>
                    <tr>
                        <td><?php echo $fila['id_ciclo']; ?></td>
                        <td><?php echo $fila['fecha_inicio']; ?></td>
                        <td><?php echo $fila['fecha_termino']; ?></td>
                        <td><?php echo $fila['fecha_inicio_cortado']; ?></td>
                        <td><?php echo $fila['fecha_termino_cortado']; ?></td>
                        <!-- <td>
                            <div class="form-group">
                                <form method="POST">
    
                                    <input type="hidden" name="id_ciclo" value="<?php echo $fila['id_ciclo']; ?>">
                                    <input type="hidden" name="fecha_inicio" value="<?php echo $fila['fecha_inicio']; ?>">
                                    <input type="hidden" name="fecha_termino" value="<?php echo $fila['fecha_termino']; ?>">
                                    <input type="hidden" name="fecha_inicio_cortado" value="<?php echo $fila['fecha_inicio_cortado']; ?>">
                                    <input type="hidden" name="fecha_termino_cortado" value="<?php echo $fila['fecha_termino_cortado']; ?>">
    
                                 <input type="submit" name="accion" value="Seleccionar" class="btn btn-primary">
                                    <input type="submit" name="accion" value="Borrar" class="btn btn-danger">
    
                                </form>
                            </div>
    
                        </td>-->
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

</div>



<?php include("../templates/pie.php"); ?>