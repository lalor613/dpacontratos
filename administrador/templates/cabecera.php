<?php
session_start();
if (isset($_SESSION['correo'])) {
} else {
    header("Location: ../administrador/index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Administrador</title>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--Jquery CDN-->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</head>

<body>

    <?php $url = "http://" . $_SERVER['HTTP_HOST'] . "/DPAcontratos"; ?>

    <nav class="navbar navbar-expand-lg text-white navbar-dark bg-primary">

        <img src="..//img/LOGO DGDSE UNACH.jpg" width="80 px" height="80 px" class="d-inline-block align-top" alt="">

        <i class="fa fa-header" aria-hidden="true">
            Universidad Autonoma de Chiapas
        </i>

        <div class="nav navbar-nav">

            <a class="nav-item nav-link text-white active" href="<?php echo $url; ?>/administrador/inicio.php">Inicio</a>
            <a class="nav-item nav-link text-white active" href="<?php echo $url; ?>/administrador/seccion/usuarios.php">Usuarios</a>
            <a class="nav-item nav-link text-white" href="<?php echo $url; ?>/administrador/seccion/ciclos_escolares.php">Ciclo</a>
            <a class="nav-item nav-link text-white" href="<?php echo $url; ?>/administrador/seccion/dependencias.php">Dependencias</a>

            <div class="dropdown open">
                <button class="btn btn-primary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Docentes
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <a href="<?php echo $url; ?>/administrador/seccion/evaluados.php">
                        <button class="dropdown-item">Evaluados</button>
                    </a>

                    <a href="<?php echo $url; ?>/administrador/seccion/titulares.php">
                        <button class="dropdown-item">Titulares</button>
                    </a>

                </div>
            </div>


            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Materias
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo $url; ?>/administrador/seccion/materias.php">Crear</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo $url; ?>/administrador/seccion/asignados_eva.php">Asignar docente</a>
                    <a class="dropdown-item" href="<?php echo $url; ?>/administrador/seccion/asignados_titulares.php">Asignar titular</a>
                </div>
            </div>
            <a class="nav-item nav-link text-white active" href="<?php echo $url; ?>/administrador/seccion/contratos.php">Crear contrato</a>
            <a class="nav-item nav-link text-white" href="<?php echo $url; ?>/administrador/cerrar.php">Cerrar sesión</a>
        </div>
    </nav>

    <div class="container-fluid">
        <br>
        <div class="row">