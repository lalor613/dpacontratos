<?php include("../DPAcontratos/templates/cabecera.php"); ?>

<div class="jumbotron">
    <h1 class="display-3">Dependencias</h1>
    <p class="lead">Administrador de contratos para el ciclo escolar ..</p>
    <hr class="my-2">
    <p>...</p>
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="#" role="button">Empezar</a>
    </p>
</div>

<?php include("../DPAcontratos/templates/pie.php") ; ?>