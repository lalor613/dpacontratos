<?php
if ($_POST) {
    header('Location: inicio.php');
} 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INICIO DE SESION</title>

  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <div class="row">

            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        INICIAR SESION
                    </div>
                    <div class="card-body">

                        <form method="POST">
                            <div class="form-group">
                                <label>Usuario</label>
                                <input type="text" name="usuario" class="form-control" placeholder="@unach.mx">
                            </div>
                            <div class="form-group">
                                <label>contraseña</label>
                                <input type="text" name="password" class="form-control" placeholder="contraseña">
                            </div>

                            <button type="submit" class="btn btn-primary">Ingresar</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>


</body>

</html>