CREATE TABLE ciclos_escolares(
	
	id_ciclo integer NOT NULL,
    fecha_inicio date,
    fecha_termino date,
    fecha_inicio_cortado date,
    fecha_termino_cortado date,
    periodo_des character varying(100),
    periodo_des_cortado character varying(100),
    PRIMARY KEY (id_ciclo)
	
);

CREATE TABLE dependencias(
	clave_depe integer NOT NULL,
    nombre_depe character varying(100),
    directivo_nom character varying(100),
    puesto character varying(100),
    fecha_nom date,
    depe_ciclo integer NOT NULL,
    PRIMARY KEY (clave_depe, depe_ciclo),
	CONSTRAINT fk_dependencia_ciclo FOREIGN KEY (depe_ciclo) REFERENCES ciclos_escolares (id_ciclo)
);

CREATE TABLE materias(

	id_mate integer NOT NULL,
	plan_est varchar(150),
    nom_materia character varying(200),
    semestre smallint,
    grupo character(10),
    hsm numeric,
	
	mate_ciclo integer,
	mate_depe integer,
    PRIMARY KEY (id_mate),
	CONSTRAINT fk_materia_depe FOREIGN KEY (mate_depe, mate_ciclo) REFERENCES dependencias (clave_depe,depe_ciclo)
);

CREATE TABLE evaluados(
	plaza_eva integer NOT NULL,
    nombre_eva character varying(150),
    edad smallint,
    curp character varying(20),
    nacionalidad character varying(50),
    ciudad character varying(150),
    colonia character varying(150),
    cp smallint,
    titulo character varying(150),
    uni_titulo character varying(150),
    fecha_titulo date,
    cedula integer,
    periodo_ini date,
    periodo_fin date,
	
    eva_depe integer NOT NULL,
    eva_ciclo integer NOT NULL,
	
    PRIMARY KEY(plaza_eva, eva_depe, eva_ciclo),
	
	CONSTRAINT fk_evaluado_dependencia FOREIGN KEY (eva_ciclo, eva_depe) REFERENCES dependencias (depe_ciclo, clave_depe)
);

CREATE TABLE titulares(
	plaza_titu integer NOT NULL,
    nombre_titu character varying(100),
    tipo_permiso character varying(100),
    motivo_per character varying(100),
	
    titu_depe integer NOT NULL,
    titu_ciclo integer NOT NULL,
	
    PRIMARY KEY (plaza_titu,titu_depe,titu_ciclo),
	CONSTRAINT fk_titular_dependencia FOREIGN KEY (titu_ciclo, titu_depe) REFERENCES dependencias (depe_ciclo, clave_depe)
);

CREATE TABLE asignados_eva(

	plaza_eva_asig integer NOT NULL,
    depe_eva_asig integer,
    ciclo_eva_asig integer,
    id_mate_eva integer NOT NULL,
	CONSTRAINT fk_asig_evaluado FOREIGN KEY (ciclo_eva_asig, depe_eva_asig, plaza_eva_asig) REFERENCES evaluados (eva_ciclo, eva_depe, plaza_eva),
	CONSTRAINT fk_asig_materia FOREIGN KEY (id_mate_eva) REFERENCES materias (id_mate)
	
);

CREATE TABLE asignados_titu(

	plaza_titu_asig integer NOT NULL,
    depe_titu_asig integer,
    ciclo_titu_asig integer,
    id_mate_titu integer NOT NULL,
	CONSTRAINT fk_asig_materia FOREIGN KEY (id_mate_titu)REFERENCES materias (id_mate),
	CONSTRAINT fk_asig_titu FOREIGN KEY (plaza_titu_asig,depe_titu_asig,ciclo_titu_asig) REFERENCES titulares(plaza_titu,titu_depe,titu_ciclo)
);

CREATE TABLE tipo_contrato(
	id INTEGER,
	nombre_tipo varchar(100),
	
	PRIMARY KEY (id)
);

CREATE TABLE contratos(
	folio integer NOT NULL,
    fkeva integer,
	fkeva_depe integer,
	fkeva_ciclo integer,
	fktitu integer,
	fktitu_depe integer,
	fktitu_ciclo integer,
	id_tipo INTEGER,
    PRIMARY KEY (folio),
	CONSTRAINT fk_cont_eva FOREIGN KEY (fkeva,fkeva_depe, fkeva_ciclo) REFERENCES evaluados (plaza_eva, eva_depe, eva_ciclo),
	CONSTRAINT fk_cont_titu FOREIGN KEY (fktitu, fktitu_depe, fktitu_ciclo)REFERENCES titulares (plaza_titu, titu_depe, titu_ciclo),
	CONSTRAINT fk_tipo_cont	FOREIGN KEY (id_tipo) REFERENCES tipo_contrato (id)	
);


Create table tipo_user(
	id integer,
	nombre_tipo varchar(100),
	
	primary key (id)
);

Create table usuarios(
	id integer,
	nombre varchar(50),
	contraseña varchar(200),
	correo varchar(50),
	tipo_id integer,
	
	depe_user integer,
	user_ciclo integer,
	
	primary key(id),
	constraint fk_id_tipouser foreign key (tipo_id) references tipo_user(id),
	constraint fk_depe_user foreign key (depe_user,user_ciclo) references dependencias(clave_depe,depe_ciclo)
);

CREATE VIEW view_usuarios_tipo as 
    select tu.nombre_tipo, 
			u.nombre, u.correo, u.tipo_id, u.depe_user, u.user_ciclo, 
			d.clave_depe, d.depe_ciclo, d.nombre_depe 
	FROM usuarios as u
    INNER JOIN tipo_user as tu 
        on u.tipo_id = tu.id 
    INNER JOIN dependencias as d
        on u.depe_user = d.clave_depe AND
            u.user_ciclo = d.depe_ciclo;
			
create view view_dependencia_cicloesc as
select * from dependencias inner join
ciclos_escolares on
dependencias.depe_ciclo = ciclos_escolares.id_ciclo;

create view view_evaluados_dependencias as
select * from evaluados as e
inner join dependencias  as d
on e.eva_depe = d.clave_depe AND
    e.eva_ciclo = d.depe_ciclo;

create view view_titulares_dependencias as
select * from titulares as t
inner join dependencias as d
on t.titu_depe = d.clave_depe AND
    t.titu_ciclo = d.depe_ciclo;

CREATE VIEW view_materias_dependencias as
SELECT * FROM materias as m
INNER JOIN dependencias as d
	on m.mate_ciclo = d.depe_ciclo AND
        m.mate_depe = d.clave_depe;
	
/* asignados_titu, materias, titulares */
/* 		at, 		m , 		t 	   */
create view view_asignados_titu as
select m.id_mate, m.plan_est,m.nom_materia, m.semestre, m.grupo, m.hsm, t.plaza_titu, t.nombre_titu, at.depe_titu_asig, at.ciclo_titu_asig
from asignados_titu as at
inner join materias as m
	on at.id_mate_titu = m.id_mate
inner join titulares as t
	on at.plaza_titu_asig = t.plaza_titu;

/*asignados_eva, materias, evaluados*/
/*		ae,			m,			e   */

create view view_asignados_eva as
select m.id_mate, m.plan_est,m.nom_materia, m.semestre, m.grupo, m.hsm, e.plaza_eva, e.nombre_eva ,ae.depe_eva_asig, ae.ciclo_eva_asig
from asignados_eva as ae
inner join materias as m
	on ae.id_mate_eva = m.id_mate
inner join evaluados as e
	on ae.plaza_eva_asig = e.plaza_eva;
